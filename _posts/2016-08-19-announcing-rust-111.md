---
title: Announcing Rust 1.11
date: 2016-08-19 10:22:55 Z
categories:
- rust
layout: post
comments: true
---

According to a report released version 1.11 of rust from the site it came booming.

> The Rust team is happy to announce the latest version of Rust, 1.11. Rust is a systems programming language focused on safety, speed, and concurrency.
> As always, you can install Rust 1.11 from the appropriate page on our website, and check out the detailed release notes for 1.11 on GitHub. 1109 patches were landed in this release.


The most interesting part of the new stable version of the file to be s Cargo.toml following types of identification are made;

```
crate-type = ["cdylib"]
```

In addition, the update is made in the library.

[link](https://blog.rust-lang.org/2016/08/18/Rust-1.11.html)
