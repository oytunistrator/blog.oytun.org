---
title: Being able to evaluate the options in a large space and fall into the trap!
date: 2017-02-09 22:00:00 Z
categories:
- life
layout: post
comments: true
---

I haven't felt myself for a long time in this space. The painful events that are experienced in the world when I look at the mistakes I made and created a space around the inside. Look at your life and meaningless in this space, just by typing the code I can't taste anymore water consume my life, my youth.


With the problems in my country, people for the sake of their job, a separate trouble has plagued me to take the risk by myself again. Now I am a monster. I accept. Tension and forget what I am and for what I need I let people put me into the vortex.

In order to be a programmer or a nice event but people see that is being used by a task to get, and simple worthless money a programmer in your own country to be disgraced, and never made a worthless asset, and that the only thing that makes.

If you are really looking very smart and one of the event from space, all the facts of life; then all you will see with your eyes and your glasses all the untruth. The real problem is that we are programmers; always in the background to allow it to take place. The most famous things they do, but successful or unsuccessful, pushing us away from being people who like a lot of background and we can't be worthless. By freeing us they all the negativity, their failures, they're trying to teach us.

They live in this world people are really incapable of doing anything. We can't do people who are successful. They are through because they're trying to lead us, and we envy them, we can draw them out.

Life it's not as simple as that sounds, but someone continues to oppress us as long as we are programmers we're going to stay always in the background. We will continue in Germany. Since we have expectations from the people around us, us people use and when they are done they like to play games on us. We gave them every opportunity we have if we're running from the truth. This life is not so simple. While we have nothing of our own potential for many reasons, assign an order to avail ourselves; if we're acting like won't order anything else.

So what we need to do is simple; get out of the system and hurting people for a moment before leave. Otherwise, all eyes on us will continue to be. We have our own corner, we will continue to write the code ourselves on our computers.

We're not doing ourselves a favour why don't we these days?

Now it's time to practice them;

1. First you are in get away from everything.
2. With all the negative attitude and negative energy in your life with negativity emitting a sample get away from everything.
3. Stop everything and let's focus on ourselves for a moment.
4. Let's stop pretending life is going on around us.
5. Just because we want to own all the systems and structures of ourselves and let it go for a while.
6. We interrogate why we were there.
7. The question is very important; yet, this system will continue to the path?
8. Any religion, language, race or thoughts to me, could be a better judge than me?
9. What I love most in my life, I did it to be free?
10. What are my favorite things and why do I enjoy them?
11. If I were to die tomorrow, what would you do to ensure their freedom to drop everything and ultimate this all day?
12. Really, I can query my life for the better?
13. If I was to love life again, I'd love to go to which City?
14. In this world that is limitless, so why bother with the restrictions?
15. Again, if I wanted to do something that I loved where I should start first?


All of these things in our lives and all we've got for you to inquire ourselves one last chance. Why aren't we using it?

Now let's all get out our social lives and social networks. If we focus only on ourselves and if we are programmers, we created our own system. Just non-free to create the things that make us different from this world we start all over again.


Now it's time to act!