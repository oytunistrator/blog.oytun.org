---
title: WTFPL About
date: '2017-03-30 11:52:03'
layout: post
categories: license
---

**The Do What The Fuck You Want To Public License (WTFPL) is a free software license.**

There is a long ongoing battle between GPL zealots and BSD fanatics, about which license type is the most free of the two. In fact, both license types have unacceptable obnoxious clauses (such as reproducing a huge disclaimer that is written in all caps) that severely restrain our freedoms. The **WTFPL** can solve this problem.

When analysing whether a license is free or not, you usually check that it allows free usage, modification and redistribution. Then you check that the additional restrictions do not impair fundamental freedoms. The **WTFPL** renders this task trivial: it allows everything and has no additional restrictions. How could life be easier? You just **DO WHAT THE FUCK YOU WANT TO.**


Full Text: [copying.txt](/txt/copying.txt)

Web Site: [wtfpl.net](http://www.wtfpl.net)
