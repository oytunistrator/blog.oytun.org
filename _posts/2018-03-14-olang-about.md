---
title: Why am I developing a language?
date: 2018-03-14 23:59:59 Z
layout: post
---

It's a story that started a few months ago.

I had the idea of writing a language with the desire to do something simpler and faster. I had many opportunities ahead of time. But I could not because of time problems. Now I have begun to develop a new language by taking advantage of the gap between them.

I decided to write the language structure. I started doing this language, taking into account the basic needs of developers. The name is "O Language". File format is based on ".ola". You can run simple scripts that you write. I am developing day to day languages ​​by adding a lot of extra features. Based functions; it usually came about with the simplification and simplification of what is needed. In addition, I see that most people write code on multiple lines. I think writing more functions on the line is extending 90% of people's work. I have developed a language that has all the definitions in one line. The writing style is somewhat similar to JavaScript and C type.

Let's write a hello world example;
```
def hello = fn () {return "Hello World!" };
hello();
```

To make it work;
```
OlangMachine ~ $ olang hello.ola
```

Result;
```
"Hello World!"
```

It's that simple. Congratulations on your first code. We will continue with examining other sections in future periods. Stay tuned for now.

O-Language About: <a href="http://o-lang.tk" target="_blank">http://o-lang.tk</a>
