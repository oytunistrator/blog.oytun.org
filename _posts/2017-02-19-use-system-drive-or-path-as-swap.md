---
title: Use system drive or path swap drive for linux/unix!
date: 2017-02-19 14:00:00 Z
categories:
- unix
layout: post
comments: true
---

I heard that it is possible to use a portion of hard disk as RAM in Windows. I wondered if this trick is available for Ubuntu, so I googled about it and couldn’t find anything related to it. But I found out an AskUbuntu answer of using USB sticks as RAM. I did the same trick on Hard Disk and it works !!
This trick can be accomplished with the use of some small commands in Terminal. The RAM memory increase can’t be noted in the System Monitor application.So, Let’s begin.
Create a file of 512 MB (The 512 indicates the RAM memory to be added):

```
dd if=/dev/zero of=~/subinsblog bs=4096 count=131072
```


The yellow background text above shows the count of the file we are going to create. This count and bs determines the file size. This is how I got the count :

```
512 * 1024^2 / 4096 = 131072
```

The yellow background text above shows the file size we need to create in Mega Bytes (MB). If you need to create SWAP space with more than 512 MB change the yellow background text above to the MB you want and get the result of the calculation. The result is the count.

Example of 1 GB :

```
1024 * 1024^2 / 4096 = 262144
```

and the command will become :

```
dd if=/dev/zero of=~/subinsblog bs=4096 count=262144
```

The command will create a file named subinsblog on your home directory.

Now let’s create the SWAP space on the file and enable the SWAP :

```
sudo mkswap ~/subinsblog -f && sudo swapon -p 1000 ~/subinsblog
```

You’re RAM Memory is now increased. To check whether if it is increased, do the following command :

```
free -m
```

Source: [How to use hard disk as ram](http://subinsb.com/how-to-use-hard-disk-as-ram-in-ubuntu-linux)
