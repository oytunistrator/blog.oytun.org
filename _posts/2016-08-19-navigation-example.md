---
title: Navigation Example with Metruga
date: 2016-08-19 14:39:13 Z
categories:
- metruga
layout: post
---

While development on the Metruga, it created a test for navigation.

```
....
m.navigation('#navbar',[
{
    path: '/index',
    link: {
        text: 'Main Page'
    },
    fn: function(){
        m.inspect.info("Main Page Loaded!");
        m.select("h1.hello").html("Main Page");
    }
},
....
],{
    class: 'nav navbar-nav',
    parent: 'ul',
    child: 'li',
    root: {
        fn: function(){
            m.inspect.info("Hello World Loaded!");
            m.select("h1.hello").html("Hello World");
        }
    }
});
....
```
If we examine the code;

```
....
m.navigation('#navbar',[
....
```

We are in the section was first created navigation navigation on HTML.

```
...
    path: '/index',
    link: {
        text: 'Main Page'
    },
    fn: function(){
        m.inspect.info("Main Page Loaded!");
        m.select("h1.hello").html("Main Page");
    }
...
```

Indicates Path address. Link properties are defined in the link. This page to go to run in the fn function.


```
...
    class: 'nav navbar-nav',
    parent: 'ul',
    child: 'li',
    root: {
        fn: function(){
            m.inspect.info("Hello World Loaded!");
            m.select("h1.hello").html("Hello World");
        }
    }
...
```

In this chapter the characteristics of navigation are defined. In this section, in addition to that get to the root of the root to do what we can define functions fn function.

So we've created with our first few lines of very simple dynamic navigation.

Src: [Metruga Getting Started](https://metru.ga/book/getting-started.html)
