---
title: Hello Slim, Goodbye Laravel and Zend!
date: 2016-12-22 19:10:00 Z
categories:
- slim
layout: post
comments: true
---

For years, after working with Zend and laravel, is full of extremely cumbersome and error I can accept that. Now, leaving aside more slim as a result of this programming with PHP framework laravel and Zend again I am continuing my career with a simple.


As someone who likes to try different things, let me introduce you a bit slim. 

Previously I've seen similar routing nodejs slim in structure. 

Gaining [composer](https://getcomposer.org/download/) if you haven't previously I would start by getting out of [here](https://getcomposer.org/download/).


Create your first application with the composer afterwards;


```
$ composer create-project slim/slim-skeleton firstapp
```


Start your application;


```
$ cd firstapp
$ php -S 0.0.0.0:8080 -t public public/index.php
```


Everything is ready, let's write a Hello World application.


```
<?php
$app = new \Slim\App($config);

$app->get('/{name}', function ($request, $response, $args) {
    return $response->write("Hello " . $args['name']);
});

$app->run();
```



So let me give you some settings. By default, the `src` folder for your application files is located.


Dependencies depencies.php is included in the file. Accordingly, layers middleware.php is in the file. For routing routes.php using the file you know. Settings settings.php is in the file. We have created a core system by using these files.


We have completed our first introduction lesson. In the next lesson we're going to tie a simple interface with databases using slim Framework Metrika and we pull data from the database. In addition, the core structure of a simple Controller we will add on a system. 


In the next lesson to discuss.
