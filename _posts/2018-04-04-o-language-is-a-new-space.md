---
title: O Language is a New Space
date: 2018-04-04 13:56:55 Z
layout: post
---

Are we ready to make a big change?

I wanted to write another moment that I expected for a long time. It is easy to change and transform things in life. Only if it is necessary to do is to discover it. The work I have done on that language for a long time has begun to yield results. Soon we are starting to create a large O language space.


- Chat with new language project: [gitter.im](https://gitter.im/olproject)
- Web site: [o-lang.tk](http://o-lang.tk/) 
- Github Page: [olproject](https://github.com/olproject)

_You can support the project to be a partner in the change._